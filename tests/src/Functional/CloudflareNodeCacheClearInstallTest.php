<?php

namespace Drupal\Tests\cloudflare_node_cc\Functional;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;

/**
 * Test the cloudflare_node_cc module install without errors.
 *
 * @group cloudflare_node_cc
 */
class CloudflareNodeCacheClearInstallTest extends WebDriverTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['cloudflare_node_cc'];

  /**
   * Assert that the cloudflare_node_cc module installed correctly.
   */
  public function testModuleInstalls() {
    // If we get here, then the module was successfully installed during the
    // setUp phase without throwing any Exceptions. Assert that TRUE is true,
    // so at least one assertion runs, and then exit.
    $this->assertTrue(TRUE, 'Module installed correctly.');
  }

}
