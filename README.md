# Cloudflare: Node Cache Clear

This module integrates with the Cloudflare API service. Gives an admin
user the ability to clear cache on a per node basis or can purge for
the whole site (zone).

For a full description of the module, visit the
[project page](https://www.drupal.org/project/cloudflare_node_cc).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/cloudflare_node_cc).


## Required

- Cloudflare SDK (will be installed via Composer).
- Cloudflare API Account (Email, API Key or Token & Zone Id is required).


## Composer Dependencies

This module depends upon Composer to install (it's dependencies).

- cloudflare sdk library - (cloudflare/sdk)


## Module Dependencies

- [Key](https://www.drupal.org/project/key)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

Installing through composer will pull in all required dependencies.

```bash
composer require drupal/cloudflare_node_cc
```

- Setup api key or token here: **/admin/config/system/keys**
- Configure Cloudflare API settings at (**/admin/config/cloudflare-node-cache-clear**)
- If purging a single node cache, please visit the edit page of the node and
  purge via action button.
- If purging whole site wide cache, go through admin toolbar tools menu item
  `"Purge Cloudflare Cache"` or via: (**/admin/cloudflare-node-cache-clear/purge-cache**)


## Drush Commands

**Flush Cache**

```bash
drush cloudflare-node-cc:flush-cache
```

Useful command for flushing zone cache or files/paths for a specific zone.

Naked command will clear the cache for the all configured zones.

Confirmation step is built-in, but is bypassed with --yes (-y).

**Arguments:**
- _--scheme_ Optional, protocol or scheme used in conjunction with domain.
Defaults to https:// if not specified.
- _--domain_ Optional, primarily for validation to ensure commands are acting
against the correctly configured domain.
- _--zone_ Optional, allows for actioning on specific zone if multiple are
present. If not provided, all configured zones will be actioned upon.
- _--path_ Optional, would allow passing in specific paths for cloudflare to
clear cache on. Useful for targeting files or specific urls. Relative to
host/domain.
- _--content-type_ Optional, allows for flushing cache for multiple node urls
of a given type.
- _--nid_ Optional, allows for flushing cache for specific node url.

**Attack Mode**

```bash
drush cloudflare-node-cc:attack-mode --zone=<zone>
```

Quickly place a zone into under attack mode.

If you wish to place the zone back into a normal mode, this must be done by
logging into Cloudflare admin interface for the zone.

**Arguments:**
- _--zone_ Required, the zone id to place in under attack mode.


## Maintainers

- George Anderson - [geoanders](https://www.drupal.org/u/geoanders)
- Michael O'Hara - [mikeohara](https://www.drupal.org/u/mikeohara)
