<?php

namespace Drupal\cloudflare_node_cc\Commands;

use Drupal\cloudflare_node_cc\CloudflareService;
use Drupal\Core\Language\LanguageInterface;
use Drupal\node\NodeInterface;
use Drupal\path_alias\AliasManager;
use Drush\Commands\DrushCommands;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Cloudflare drush commands.
 */
class CloudflareCommands extends DrushCommands {

  /**
   * The Cloudflare service.
   *
   * @var \Drupal\cloudflare_node_cc\CloudflareService
   */
  protected $cloudflareService;

  /**
   * The path alias manager.
   *
   * @var \Drupal\path_alias\AliasManager
   */
  protected $aliasManager;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The constructor.
   *
   * @param \Drupal\cloudflare_node_cc\CloudflareService $cloudflare_service
   *   The cloudflare service.
   * @param \Drupal\path_alias\AliasManager $alias_manager
   *   The alias manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(CloudflareService $cloudflare_service, AliasManager $alias_manager, RequestStack $request_stack) {
    parent::__construct();
    $this->cloudflareService = $cloudflare_service;
    $this->aliasManager = $alias_manager;
    $this->requestStack = $request_stack;
  }

  /**
   * Flush Cloudflare zone cache or files.
   *
   * Provides several options for flushing zone cache and it's files.
   *
   * @param array $options
   *   The flush options.
   *
   * @command cloudflare-node-cc:flush-cache
   * @aliases cfncc-cr,cfncc-flush,cfncc-flush-cache
   *
   * @options scheme The protocol or scheme used in conjunction with domain.
   * @options domain The Cloudflare domain to flush cache or files/paths.
   * @options zone The Cloudflare zone to flush cache or files/paths.
   * @options path The relative path or file to be flushed. Multiple paths
   *   allowed or can be defined.
   * @options content-type The node content type to filter nodes by for
   *   flushing multiple node urls.
   * @options nid The specific node for flushing node url.
   *
   * @usage cloudflare-node-cc:flush-cache -y
   * @usage cloudflare-node-cc:flush-cache --domain=<domain> --zone=<zone_id>
   *   --path=<path>
   * @usage cloudflare-node-cc:flush-cache --domain=<domain> --zone=<zone_id>
   * @usage cloudflare-node-cc:flush-cache --content-type=<content-type>
   *   --nid=<nid>
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException|\Drupal\Core\Entity\EntityMalformedException
   */
  public function flushCache(
    array $options = [
      'scheme' => 'https://',
      'domain' => NULL,
      'zone' => NULL,
      'path' => [],
      'content-type' => NULL,
      'nid' => NULL,
    ]
  ) {
    // Check to see if multi zone is configured.
    if ($this->cloudflareService->isMultiZone()) {

      // Get zones per language.
      $cf_lang_zones = $this->cloudflareService->getLanguageZones();
      foreach ($cf_lang_zones as $lang_code => $cf_zone_id) {
        // Check for configured zone id.
        if (empty($cf_zone_id)) {
          $this->io()->error('Cloudflare zone id not configured.');
          continue;
        }

        // Check passed in zone id. Make sure it matches or we skip.
        if (!empty($options['zone']) && $options['zone'] != $cf_zone_id) {
          continue;
        }

        // Get zone information.
        $cf_zone = $this->cloudflareService->getZone($cf_zone_id);
        if (!$cf_zone) {
          $this->io()->error('Cloudflare zone was not found.');
          continue;
        }

        // Check zone domain.
        $cf_domain = $options['scheme'] . $cf_zone->name;
        // Language domain should be the same as zone domain?
        $cf_lang_domain = $this->cloudflareService->getLanguageDomain($lang_code);
        if ($cf_domain != $cf_lang_domain) {
          continue;
        }
        // Check passed in domain.
        if (!empty($options['domain'])) {
          if (!in_array($options['domain'], [$cf_domain, $cf_zone->name])) {
            continue;
          }
        }

        // Check paths/files. If paths are defined, don't purge zone cache.
        // If content-type or node id are defined, don't purge zone cache.
        if (empty($options['path']) && empty($options['content-type']) && empty($options['nid'])) {
          // Confirm with user to purge zone cache.
          if ($this->io()
            ->confirm('Are you sure you wish to purge Cloudflare zone: ' . $cf_zone->name . '?')) {
            if ($this->cloudflareService->purgeZoneCache($cf_zone_id)) {
              $this->io()->success('Cloudflare zone: ' . $cf_zone->name . ' cache was purged successfully.');
            }
            else {
              $this->io()->error('Cloudflare zone: ' . $cf_zone->name . ' cache failed to purge.');
            }
          }
        }
        else {
          // Store paths/files to purge.
          $files = [];

          // Paths are defined. Let's purge defined paths/files.
          if (!empty($options['path'])) {
            foreach ($options['path'] as $file) {
              $files[] = rtrim($cf_domain, '/') . $file;
            }
          }

          // Check for content-type or node id.
          if (!empty($options['content-type']) || !empty($options['nid'])) {
            $properties = [
              'status' => NodeInterface::PUBLISHED,
            ];
            if (!empty($options['content-type'])) {
              $properties['type'] = $options['content-type'];
            }
            if (!empty($options['nid'])) {
              $properties['nid'] = $options['nid'];
            }
            // Get nodes.
            $nodes = $this->cloudflareService->getEntityTypeManager()->getStorage('node')->loadByProperties($properties);
            /** @var \Drupal\node\Entity\Node $node */
            foreach ($nodes as $node) {
              if ($node->hasTranslation($lang_code)) {
                $url_alias = $this->aliasManager->getAliasByPath('/node/' . $node->id(), $lang_code);
                $files[] = rtrim($cf_domain, '/') . $url_alias;
              }
              if ($node->hasTranslation(LanguageInterface::LANGCODE_NOT_APPLICABLE)) {
                $url_alias = $this->aliasManager->getAliasByPath('/node/' . $node->id(), LanguageInterface::LANGCODE_NOT_APPLICABLE);
                $files[] = rtrim($cf_domain, '/') . $url_alias;
              }
            }
          }

          // Make sure we have files to purge.
          if (!empty($files)) {
            // Print out table of files that will be purged.
            $rows = [];
            foreach ($files as $file) {
              $rows[] = [$file];
            }
            $this->io()->table(['File(s) or path(s) to be purged'], $rows);

            // Confirm with user to purge files.
            if ($this->io()
              ->confirm('Are you sure you wish to purge the paths above for Cloudflare zone: ' . $cf_zone->name . ' ?')) {
              if ($this->cloudflareService->purgeZoneCacheFiles($cf_zone_id, $files)) {
                $this->io()->success('Cloudflare zone: ' . $cf_zone->name . ' cache paths purged successfully.');
              }
              else {
                $this->io()->error('Cloudflare zone: ' . $cf_zone->name . ' cache paths failed to purge.');
              }
            }
          }
        }
      }
    }
    else {
      // Make sure we have configured zone id.
      $cf_zone_id = $this->cloudflareService->getZoneId();
      if (empty($cf_zone_id)) {
        $this->io()->error('Cloudflare zone id not configured.');
        return;
      }

      // Check passed in zone id.
      if (!empty($options['zone']) && $options['zone'] != $cf_zone_id) {
        $this->io()->error('Passed in Cloudflare zone id not matching configured.');
        return;
      }

      // Get zone information.
      $cf_zone = $this->cloudflareService->getZone($cf_zone_id);
      if (!$cf_zone) {
        $this->io()->error('Cloudflare zone was not found.');
        return;
      }

      // Check domain.
      $cf_domain = $options['scheme'] . $cf_zone->name;
      if (!empty($options['domain'])) {
        if (!in_array($options['domain'], [$cf_domain, $cf_zone->name])) {
          return;
        }
      }

      // Check paths/files. If paths are defined, don't purge zone cache.
      // If content-type or node id are defined, don't purge zone cache.
      if (empty($options['path']) && empty($options['content-type']) && empty($options['nid'])) {
        // Confirm with user to purge zone cache.
        if ($this->io()
          ->confirm('Are you sure you wish to purge Cloudflare zone: ' . $cf_zone->name . '?')) {
          if ($this->cloudflareService->purgeZoneCache($cf_zone_id)) {
            $this->io()->success('Cloudflare zone: ' . $cf_zone->name . ' cache was purged successfully.');
          }
          else {
            $this->io()->error('Cloudflare zone: ' . $cf_zone->name . ' cache failed to purge.');
          }
        }
      }
      else {
        $files = [];

        // Paths are defined. Let's purge defined paths/files.
        if (!empty($options['path'])) {
          foreach ($options['path'] as $file) {
            $files[] = rtrim($cf_domain, '/') . $file;
          }
        }

        // Check for content-type or node id.
        if (!empty($options['content-type']) || !empty($options['nid'])) {
          $properties = [
            'status' => NodeInterface::PUBLISHED,
          ];
          if (!empty($options['content-type'])) {
            $properties['type'] = $options['content-type'];
          }
          if (!empty($options['nid'])) {
            $properties['nid'] = $options['nid'];
          }
          // Get nodes.
          $nodes = $this->cloudflareService->getEntityTypeManager()->getStorage('node')->loadByProperties($properties);
          /** @var \Drupal\node\Entity\Node $node */
          foreach ($nodes as $node) {
            $translation_languages = $node->getTranslationLanguages();
            foreach ($translation_languages as $lang_code => $language) {
              if ($node->hasTranslation($lang_code)) {
                $files[] = $node->getTranslation($lang_code)->toUrl()->setAbsolute()->toString();
              }
            }
          }
        }

        // Make sure we have files.
        if (!empty($files)) {
          // Print out table of files that will be purged.
          $rows = [];
          foreach ($files as $file) {
            $rows[] = [$file];
          }
          $this->io()->table(['File(s) or path(s) to be purged'], $rows);

          // Confirm with user to purge files.
          if ($this->io()
            ->confirm('Are you sure you wish to purge paths above for Cloudflare zone: ' . $cf_zone->name . ' ?')) {
            if ($this->cloudflareService->purgeZoneCacheFiles($cf_zone_id, $files)) {
              $this->io()->success('Cloudflare zone:' . $cf_zone->name . ' cache paths purged successfully.');
            }
            else {
              $this->io()->error('Cloudflare zone: ' . $cf_zone->name . ' cache paths failed to purge.');
            }
          }
        }
      }
    }
  }

  /**
   * Set Cloudflare zone security level to attack mode.
   *
   * @param array $options
   *   The options.
   *
   * @command cloudflare-node-cc:attack-mode
   * @aliases cfncc-at,cfncc-attack-mode
   * @options zone The Cloudflare zone id to update security level (required).
   * @usage cloudflare-node-cc:attack-mode --zone=<zone_id> -y
   * @usage cloudflare-node-cc:attack-mode --zone=<zone_id>
   */
  public function attackMode(
    array $options = [
      'zone' => NULL,
    ]
  ) {
    // Make sure zone is set.
    if (empty($options['zone'])) {
      $this->io()->error('Cloudflare zone id is required. Please provide valid zone id.');
      return;
    }
    // Check to see if multi zone is configured.
    if ($this->cloudflareService->isMultiZone()) {

      // Get zones per language.
      $cf_lang_zones = $this->cloudflareService->getLanguageZones();
      foreach ($cf_lang_zones as $cf_zone_id) {

        // Check for configured zone id.
        if (empty($cf_zone_id)) {
          $this->io()->error('Cloudflare zone id not configured.');
          continue;
        }

        // Make sure zone id matches before proceeding.
        if ($cf_zone_id != $options['zone']) {
          continue;
        }

        // Get zone information.
        $cf_zone = $this->cloudflareService->getZone($cf_zone_id);
        if (!$cf_zone) {
          $this->io()->error('Cloudflare zone was not found.');
          continue;
        }

        // Make sure current host matches zone host/domain.
        if ($this->requestStack->getCurrentRequest()->getHost() != $cf_zone->name) {
          $this->io()->error('Cloudflare zone domain and request domain do not match.');
          continue;
        }

        // Make sure not already in "under_attack" mode.
        $cf_security_level = $this->cloudflareService->getZoneSecurityLevel($cf_zone_id);
        if ($cf_security_level != 'under_attack') {

          // Confirm user wishes to put zone into attack mode.
          if ($this->io()
            ->confirm('Set security level to attack mode for Cloudflare zone: ' . $cf_zone->name . '?')) {
            if ($this->cloudflareService->setZoneSecurityLevel($cf_zone_id, 'under_attack')) {
              $this->io()->success('Cloudflare zone: ' . $cf_zone->name . ' security level updated successfully.');
            }
            else {
              $this->io()->error('Cloudflare zone: ' . $cf_zone->name . ' failed to update security level.');
            }
          }
        }
        else {
          $this->io()->warning('Cloudflare zone: ' . $cf_zone->name . ' already in attack mode.');
        }
      }
    }
    else {
      // Make sure we have configured zone id.
      $cf_zone_id = $this->cloudflareService->getZoneId();
      if (empty($cf_zone_id)) {
        $this->io()->error('Cloudflare zone id not configured.');
        return;
      }

      // Check passed in zone id.
      if ($options['zone'] != $cf_zone_id) {
        $this->io()->error('Passed in Cloudflare zone id not matching configured.');
        return;
      }

      // Get zone information.
      $cf_zone = $this->cloudflareService->getZone($cf_zone_id);
      if (!$cf_zone) {
        $this->io()->error('Cloudflare zone was not found.');
        return;
      }

      // Make sure current host matches zone host/domain.
      if ($this->requestStack->getCurrentRequest()->getHost() != $cf_zone->name) {
        $this->io()->error('Cloudflare zone domain and request host do not match.');
        return;
      }

      // Make sure not already in "under_attack" mode.
      $cf_security_level = $this->cloudflareService->getZoneSecurityLevel($cf_zone_id);
      if ($cf_security_level != 'under_attack') {

        // Confirm user wishes to put zone into attack mode.
        if ($this->io()
          ->confirm('Set security level to attack mode for Cloudflare zone: ' . $cf_zone->name . '?')) {
          if ($this->cloudflareService->setZoneSecurityLevel($cf_zone_id, 'under_attack')) {
            $this->io()->success('Cloudflare zone: ' . $cf_zone->name . ' security level updated successfully.');
          }
          else {
            $this->io()->error('Cloudflare zone: ' . $cf_zone->name . ' failed to update security level.');
          }
        }
      }
      else {
        $this->io()->warning('Cloudflare zone: ' . $cf_zone->name . ' already in attack mode.');
      }
    }
  }

}
