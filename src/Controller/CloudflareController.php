<?php

namespace Drupal\cloudflare_node_cc\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\cloudflare_node_cc\CloudflareService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * The Cloudflare Controller.
 *
 * @package Drupal\cloudflare_node_cc\Controller
 */
class CloudflareController extends ControllerBase {

  /**
   * The request.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The cloudflare service.
   *
   * @var \Drupal\cloudflare_node_cc\CloudflareService
   */
  protected $cloudflareService;

  /**
   * CloudflareController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request.
   * @param \Drupal\cloudflare_node_cc\CloudflareService $cloudflare_service
   *   The cloudflare service.
   */
  public function __construct(RequestStack $request_stack, CloudflareService $cloudflare_service) {
    $this->requestStack = $request_stack;
    $this->cloudflareService = $cloudflare_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('cloudflare_node_cc.cloudflare_service')
    );
  }

  /**
   * Reload the previous page.
   *
   * @return mixed|string
   *   Returns the referrer or front page.
   */
  public function reloadPage() {
    $request = $this->requestStack->getCurrentRequest();
    if ($request->server->get('HTTP_REFERER')) {
      return $request->server->get('HTTP_REFERER');
    }
    else {
      return '/';
    }
  }

  /**
   * Purge everything in Cloudflare zones.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns the redirect response.
   */
  public function purgeCache(): RedirectResponse {
    $cf_zones_to_purge = [];
    // Language zones (multiple zones)
    if ($this->cloudflareService->isMultiZone()) {
      $cf_lang_zones = $this->cloudflareService->getLanguageZones();
      if (!empty($cf_lang_zones)) {
        foreach ($cf_lang_zones as $cf_lang_zone_id) {
          if (!in_array($cf_lang_zone_id, $cf_zones_to_purge)) {
            $cf_zones_to_purge[] = $cf_lang_zone_id;
          }
        }
      }
    }
    else {
      // Default/single zone id.
      $cf_default_zone_id = $this->cloudflareService->getZoneId();
      if (!empty($cf_default_zone_id)) {
        $cf_zones_to_purge[] = $cf_default_zone_id;
      }
    }
    // Purge zones in array.
    if (!empty($cf_zones_to_purge)) {
      foreach ($cf_zones_to_purge as $cf_zone_id) {
        $cf_zone = $this->cloudflareService->getZone($cf_zone_id);
        if ($this->cloudflareService->purgeZoneCache($cf_zone_id)) {
          $this->messenger()
            ->addMessage($this->t('Cloudflare zone: %zone cache purged.', ['%zone' => ($cf_zone && !empty($cf_zone->name)) ? $cf_zone->name : '']));
        }
        else {
          $this->messenger()
            ->addError($this->t('Cloudflare zone: %zone cache purge failed. Please see logs for more details.', ['%zone' => ($cf_zone && !empty($cf_zone->name)) ? $cf_zone->name : '']));
        }
      }
    }
    return new RedirectResponse($this->reloadPage());
  }

}
