<?php

namespace Drupal\cloudflare_node_cc\Form;

use Drupal\cloudflare_node_cc\CloudflareService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Cloudflare Config Form class.
 *
 * @package Drupal\cloudflare_node_cc\Form
 */
class CloudflareConfigForm extends ConfigFormBase {

  /**
   * The Cloudflare service.
   *
   * @var \Drupal\cloudflare_node_cc\CloudflareService
   */
  protected $cloudflareService;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * CloudflareConfigForm constructor.
   *
   * @param \Drupal\cloudflare_node_cc\CloudflareService $cloudflare_service
   *   The Cloudflare service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config factory service.
   * @param \Drupal\Core\Language\LanguageManager $language_manager
   *   The Language manager service.
   */
  public function __construct(
    CloudflareService $cloudflare_service,
    ConfigFactoryInterface $config_factory,
    LanguageManager $language_manager
  ) {
    parent::__construct($config_factory);
    $this->cloudflareService = $cloudflare_service;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cloudflare_node_cc.cloudflare_service'),
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudflare_node_cc.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_node_cc_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $state_config = $this->cloudflareService->getStateConfig();
    $form['#tree'] = TRUE;

    $form['api'] = [
      '#type' => 'details',
      '#title' => $this->t('Cloudflare API Settings'),
      '#open' => TRUE,
    ];

    $form['api']['cloudflare_auth_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Cloudflare Auth Type'),
      '#options' => [
        'api_key' => $this->t('Email/API Key'),
        'api_token' => $this->t('API Token'),
      ],
      '#empty_option' => $this->t('-- Select Auth Type --'),
      '#required' => TRUE,
      '#default_value' => !empty($state_config['cloudflare_auth_type']) ? $state_config['cloudflare_auth_type'] : 'api_key',
    ];

    $form['api']['cloudflare_api_token_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API Token'),
      '#description' => $this->t('Cloudflare API token.'),
      '#empty_option' => $this->t('- Select Token -'),
      '#default_value' => $state_config['cloudflare_api_token_name'] ?? '',
      '#key_filters' => ['type' => 'authentication'],
      '#states' => [
        'visible' => [
          ':input[name="api[cloudflare_auth_type]"]' => ['value' => 'api_token'],
        ],
        'required' => [
          ':input[name="api[cloudflare_auth_type]"]' => ['value' => 'api_token'],
        ],
      ],
    ];

    $form['api']['cloudflare_email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address'),
      '#description' => $this->t('Cloudflare API email address.'),
      '#default_value' => !empty($state_config['cloudflare_email']) ? $state_config['cloudflare_email'] : '',
      '#states' => [
        'visible' => [
          ':input[name="api[cloudflare_auth_type]"]' => ['value' => 'api_key'],
        ],
        'required' => [
          ':input[name="api[cloudflare_auth_type]"]' => ['value' => 'api_key'],
        ],
      ],
    ];

    $form['api']['cloudflare_api_key_name'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API Key'),
      '#description' => $this->t('Cloudflare API key.'),
      '#empty_option' => $this->t('- Select Key -'),
      '#default_value' => $state_config['cloudflare_api_key_name'] ?? '',
      '#key_filters' => ['type' => 'authentication'],
      '#states' => [
        'visible' => [
          ':input[name="api[cloudflare_auth_type]"]' => ['value' => 'api_key'],
        ],
        'required' => [
          ':input[name="api[cloudflare_auth_type]"]' => ['value' => 'api_key'],
        ],
      ],
    ];

    $form['api']['cloudflare_multi_zone'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Multiple Cloudflare Zones.'),
      '#default_value' => $state_config['cloudflare_multi_zone'] ?? FALSE,
      '#description' => $this->t('Enable multiple Cloudflare zones and map per language.'),
    ];

    // Single or multiple zones depending on language.
    $languages = $this->languageManager->getLanguages();

    $form['api']['cloudflare_zone_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Zone Id'),
      '#description' => $this->t('Cloudflare API zone id.'),
      '#default_value' => !empty($state_config['cloudflare_zone_id']) ? $state_config['cloudflare_zone_id'] : '',
      '#states' => [
        'visible' => [
          ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => FALSE],
        ],
        'required' => [
          ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['api']['cloudflare_lang_zones'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Cloudflare Language Zones'),
      '#states' => [
        'visible' => [
          ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $cf_lang_zones = $state_config['cloudflare_lang_zones'] ?? [];
    $form['api']['cloudflare_lang_domains'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Cloudflare Language Domains'),
      '#states' => [
        'visible' => [
          ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $cf_lang_domains = $state_config['cloudflare_lang_domains'] ?? [];
    foreach ($languages as $language) {
      $form['api']['cloudflare_lang_zones'][$language->getId()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Zone Id (%language)', ['%language' => $language->getName()]),
        '#default_value' => $cf_lang_zones[$language->getId()] ?? '',
        '#description' => $this->t('Cloudflare Zone Id For %language.', ['%language' => $language->getName()]),
        '#states' => [
          'visible' => [
            ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => TRUE],
          ],
        ],
      ];
      $form['api']['cloudflare_lang_domains'][$language->getId()] = [
        '#type' => 'textfield',
        '#title' => $this->t('Zone Domain (%language)', ['%language' => $language->getName()]),
        '#default_value' => $cf_lang_domains[$language->getId()] ?? '',
        '#description' => $this->t('Cloudflare Zone Domain For %language.', ['%language' => $language->getName()]),
        '#states' => [
          'visible' => [
            ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => TRUE],
          ],
          'required' => [
            ':input[name="api[cloudflare_multi_zone]"]' => ['checked' => TRUE],
          ],
        ],
      ];
    }

    $form['api']['cloudflare_restore_client_ip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Restore Client Ip Address'),
      '#description' => $this->t('Cloudflare operates as a reverse proxy and replaces the client ip address. This setting will restore it.<br /> Read more <a href="https://support.cloudflare.com/hc/en-us/articles/200170986-How-does-CloudFlare-handle-HTTP-Request-headers-">here</a>.'),
      '#default_value' => !empty($state_config['cloudflare_restore_client_ip']) ? $state_config['cloudflare_restore_client_ip'] : FALSE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();

    $cf_zone_id = $values['api']['cloudflare_zone_id'];
    $cf_lang_zones = $values['api']['cloudflare_lang_zones'];
    $cf_email = $values['api']['cloudflare_email'];
    $cf_multi_zone = $values['api']['cloudflare_multi_zone'];

    $cf_api_key_name = $values['api']['cloudflare_api_key_name'];
    $cf_api_token_name = $values['api']['cloudflare_api_token_name'];

    $cf_api_key = $this->cloudflareService->getApiKey($cf_api_key_name);
    $cf_api_token = $this->cloudflareService->getApiToken($cf_api_token_name);
    if ($cf_api_key || $cf_api_token) {

      // Get valid zones.
      $cf_zones = $this->cloudflareService->getZones($cf_email, $cf_api_key, $cf_api_token);
      $cf_zone_options = [];
      if (!empty($cf_zones)) {
        foreach ($cf_zones as $cf_zone) {
          $cf_zone_options[$cf_zone->id] = $cf_zone->name;
        }
      }

      // Validate default zone id.
      if (!empty($cf_zone_id)) {
        if (!array_key_exists($cf_zone_id, $cf_zone_options)) {
          $form_state->setErrorByName('cloudflare_zone_id', $this->t('Default Cloudflare zone id is invalid. Please enter in valid zone id.'));
        }
      }

      // Validate language zones.
      if (!empty($cf_lang_zones) && $cf_multi_zone) {
        foreach ($cf_lang_zones as $lang_code => $lang_zone_id) {
          if (!array_key_exists($lang_zone_id, $cf_zone_options)) {
            $form_state->setErrorByName('cloudflare_lang_zones][' . $lang_code, $this->t('Zone id for language %language is invalid. Please enter in valid zone id.', ['%language' => $lang_code]));
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $configData = [
      'cloudflare_email' => $values['api']['cloudflare_email'],
      'cloudflare_auth_type' => $values['api']['cloudflare_auth_type'],
      'cloudflare_api_key_name' => $values['api']['cloudflare_api_key_name'],
      'cloudflare_api_token_name' => $values['api']['cloudflare_api_token_name'],
      'cloudflare_multi_zone' => $values['api']['cloudflare_multi_zone'],
      'cloudflare_zone_id' => $values['api']['cloudflare_zone_id'],
      'cloudflare_lang_zones' => $values['api']['cloudflare_lang_zones'],
      'cloudflare_lang_domains' => $values['api']['cloudflare_lang_domains'],
      'cloudflare_restore_client_ip' => $values['api']['cloudflare_restore_client_ip'],
    ];

    // Save state config.
    $this->cloudflareService->setStateConfig($configData);
  }

}
