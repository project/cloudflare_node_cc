<?php

namespace Drupal\cloudflare_node_cc\EventSubscriber;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\cloudflare_node_cc\CloudflareService;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Psr\Log\LoggerInterface;

/**
 * The Cloudflare Client Ip Restore Class.
 *
 * Restores the true client Ip address.
 *
 * @see https://support.cloudflare.com/hc/en-us/articles/200170986-How-does-CloudFlare-handle-HTTP-Request-headers-
 *
 * @package Drupal\cloudflare_node_cc\EventSubscriber
 */
class CloudflareClientIpRestore implements EventSubscriberInterface {

  use StringTranslationTrait;

  const CLOUDFLARE_RANGE_KEY = 'cloudflare_range_key';

  /**
   * The Cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * The cloudflare service.
   *
   * @var \Drupal\cloudflare_node_cc\CloudflareService
   */
  protected $cloudflareService;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TRUE/FALSE if client ip restoration enabled.
   *
   * @var bool
   */
  protected $isClientIpRestoreEnabled;

  /**
   * CloudflareClientIpRestore constructor.
   *
   * @param \Drupal\cloudflare_node_cc\CloudflareService $cloudflare_service
   *   The cloudflare service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The http client.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(CloudflareService $cloudflare_service, CacheBackendInterface $cache, ClientInterface $http_client, LoggerInterface $logger) {
    $this->httpClient = $http_client;
    $this->cache = $cache;
    $this->cloudflareService = $cloudflare_service;
    $this->logger = $logger;

    $config = $this->cloudflareService->getStateConfig();
    $this->isClientIpRestoreEnabled = FALSE;
    if (!empty($config['cloudflare_restore_client_ip'])) {
      $this->isClientIpRestoreEnabled = TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['onRequest', 20];
    return $events;
  }

  /**
   * Restores the origination client IP delivered to Drupal from CloudFlare.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent|\Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The response event.
   */
  public function onRequest($event) {

    // Make sure client ip restore is enabled first.
    if (!$this->isClientIpRestoreEnabled) {
      return;
    }

    $current_request = $event->getRequest();

    // CF-Connecting-IP
    // Provides the original client (visitor) IP address to the
    // origin web server.
    $cf_connecting_ip = $current_request->server->get('HTTP_CF_CONNECTING_IP');
    $has_http_cf_connecting_ip = !empty($cf_connecting_ip);

    if (!$has_http_cf_connecting_ip) {
      $message = $this->t("Request came through without being routed through CloudFlare.");
      $this->logger->warning($message);
      return;
    }

    // As the changed remote address will make it impossible to determine
    // a trusted proxy, we need to make sure we set the right protocol as well.
    // @see \Symfony\Component\HttpFoundation\Request::isSecure()
    $event->getRequest()->server->set('HTTPS', $event->getRequest()
      ->isSecure() ? 'on' : 'off');
    $event->getRequest()->server->set('REMOTE_ADDR', $cf_connecting_ip);
    $event->getRequest()->overrideGlobals();
  }

}
