<?php

namespace Drupal\cloudflare_node_cc;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\key\KeyRepository;
use GuzzleHttp\Exception\RequestException;

use Cloudflare\API\Auth\APIKey;
use Cloudflare\API\Auth\APIToken;
use Cloudflare\API\Adapter\Guzzle;
use Cloudflare\API\Endpoints\FirewallSettings;
use Cloudflare\API\Endpoints\Zones;
use Cloudflare\API\Endpoints\User;

/**
 * Cloudflare service class.
 *
 * @package Drupal\cloudflare_node_cc
 */
class CloudflareService {

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The time.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The key repository object.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * CloudflareService constructor.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The config factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   * @param \Drupal\key\KeyRepository $keyRepository
   *   Key repository object.
   */
  public function __construct(
    LoggerChannelFactoryInterface $loggerFactory,
    ConfigFactoryInterface $config,
    StateInterface $state,
    TimeInterface $time,
    EntityTypeManagerInterface $entityTypeManager,
    MessengerInterface $messenger,
    KeyRepository $keyRepository
  ) {
    $this->logger = $loggerFactory->get('cloudflare_node_cc');
    $this->config = $config->get('cloudflare_node_cc.settings');
    $this->state = $state;
    $this->time = $time;
    $this->entityTypeManager = $entityTypeManager;
    $this->messenger = $messenger;
    $this->keyRepository = $keyRepository;
  }

  /**
   * Get configuration.
   *
   * @return \Drupal\Core\Config\Config|null
   *   Returns the config.
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * Get state configuration.
   *
   * @return array
   *   Returns the state config.
   */
  public function getStateConfig(): array {
    $state_keys = [
      'cloudflare_email',
      'cloudflare_auth_type',
      'cloudflare_api_key_name',
      'cloudflare_api_token_name',
      'cloudflare_multi_zone',
      'cloudflare_zone_id',
      'cloudflare_lang_zones',
      'cloudflare_lang_domains',
      'cloudflare_restore_client_ip',
    ];
    return $this->state->getMultiple($state_keys);
  }

  /**
   * Set state configuration data.
   *
   * @param array $data
   *   The state config data.
   */
  public function setStateConfig(array $data) {
    $this->state->setMultiple($data);
  }

  /**
   * Get entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   Returns the entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager;
  }

  /**
   * Get Cloudflare API authentication.
   *
   * @return \Cloudflare\API\Auth\APIKey|\Cloudflare\API\Auth\APIToken|null
   *   Returns the Cloudflare API authentication.
   */
  public function getApiAuth() {
    $cf_email = $this->state->get('cloudflare_email');
    $cf_api_key = $this->getApiKey();
    $cf_api_token = $this->getApiToken();
    $cf_auth = NULL;

    // API Key authentication.
    if (!empty($cf_email) && !empty($cf_api_key)) {
      $cf_auth = new APIKey($cf_email, $cf_api_key);
    }

    // API Token authentication.
    if (!empty($cf_api_token) && !$cf_auth) {
      $cf_auth = new APIToken($cf_api_token);
    }

    return $cf_auth;
  }

  /**
   * Get Cloudflare API key.
   *
   * @param string $cf_key_name
   *   The key name.
   *
   * @return mixed|string
   *   Returns the API key.
   */
  public function getApiKey(string $cf_key_name = '') {
    $key = '';
    $cf_key_name = $cf_key_name ?: $this->state->get('cloudflare_api_key_name');
    if (!empty($cf_key_name)) {
      $key = $this->keyRepository->getKey($cf_key_name)->getKeyValue();
    }
    return $key;
  }

  /**
   * Get Cloudflare API token.
   *
   * @param string $cf_token_name
   *   The key/token name.
   *
   * @return mixed|string
   *   Returns the API token.
   */
  public function getApiToken(string $cf_token_name = '') {
    $token = '';
    $cf_token_name = $cf_token_name ?: $this->state->get('cloudflare_api_token_name');
    if (!empty($cf_token_name)) {
      $token = $this->keyRepository->getKey($cf_token_name)->getKeyValue();
    }
    return $token;
  }

  /**
   * Get Cloudflare zones.
   *
   * @param string $cf_email
   *   The email.
   * @param string $cf_api_key
   *   The api key.
   * @param string $cf_api_token
   *   The api token.
   *
   * @return array
   *   Returns an array of zones.
   */
  public function getZones(string $cf_email = '', string $cf_api_key = '', string $cf_api_token = ''): array {
    $zones = [];

    if (empty($cf_email)) {
      $cf_email = $this->state->get('cloudflare_email');
    }

    if (empty($cf_api_key)) {
      $cf_api_key = $this->getApiKey();
    }

    if (empty($cf_api_token)) {
      $cf_api_token = $this->getApiToken();
    }

    $cf_auth = NULL;

    // API Key authentication.
    if (!empty($cf_email) && !empty($cf_api_key)) {
      $cf_auth = new APIKey($cf_email, $cf_api_key);
    }

    // API Token authentication.
    if (!empty($cf_api_token) && !$cf_auth) {
      $cf_auth = new APIToken($cf_api_token);
    }

    if ($cf_auth) {
      $cf_adapter = new Guzzle($cf_auth);
      try {
        $cf_zones = new Zones($cf_adapter);
        if (!empty($cf_zones)) {
          $page = 1;
          $zones_per_page = 50;
          while ($results = $cf_zones->listZones('', '', $page, $zones_per_page)->result) {
            if (is_array($results)) {
              foreach ($results as $zone) {
                $zones[$zone->id] = $zone;
              }
              $page++;
            }
          }
        }
      }
      catch (RequestException | \Exception $e) {
        watchdog_exception('cloudflare_node_cc', $e);
      }
    }

    return $zones;
  }

  /**
   * Get Cloudflare zone.
   *
   * @param string $zone_id
   *   The zone id.
   *
   * @return mixed|null
   *   Returns the zone.
   */
  public function getZone(string $zone_id) {
    $zone = NULL;
    $cf_zones = $this->getZones();
    if (!empty($cf_zones) && !empty($cf_zones[$zone_id])) {
      $zone = $cf_zones[$zone_id];
    }
    return $zone;
  }

  /**
   * Cloudflare purge everything in zone cache.
   *
   * @param string $zone_id
   *   The zone id.
   *
   * @return bool
   *   Returns purge status.
   */
  public function purgeZoneCache(string $zone_id): bool {
    $status = FALSE;

    $cf_auth = $this->getApiAuth();
    if ($cf_auth) {
      $cf_adapter = new Guzzle($cf_auth);
      try {
        $cf_zones = new Zones($cf_adapter);
        $status = $cf_zones->cachePurgeEverything($zone_id);
      }
      catch (RequestException | \Exception $e) {
        watchdog_exception('cloudflare_node_cc', $e);
      }
    }

    return $status;
  }

  /**
   * Cloudflare purge specific files in zone cache.
   *
   * @param string $zone_id
   *   The zone id.
   * @param array $files
   *   The files to purge.
   *
   * @return bool
   *   The purge status.
   */
  public function purgeZoneCacheFiles(string $zone_id, array $files = []): bool {
    $status = FALSE;

    $cf_auth = $this->getApiAuth();
    if ($cf_auth) {
      $cf_adapter = new Guzzle($cf_auth);
      try {
        $cf_zones = new Zones($cf_adapter);
        $status = $cf_zones->cachePurge($zone_id, $files);
      }
      catch (RequestException | \Exception $e) {
        watchdog_exception('cloudflare_node_cc', $e);
      }
    }

    return $status;
  }

  /**
   * Get Cloudflare user id.
   *
   * @return null|string
   *   Returns the user id.
   */
  public function getUserId(): ?string {
    $user_id = NULL;

    $cf_auth = $this->getApiAuth();
    if ($cf_auth) {
      $cf_adapter = new Guzzle($cf_auth);
      $cf_user = new User($cf_adapter);
      if (!empty($cf_user)) {
        $user_id = $cf_user->getUserID();
      }
    }

    return $user_id;
  }

  /**
   * Get Cloudflare zone security level.
   *
   * @param string $zone_id
   *   The Cloudflare zone id.
   *
   * @return false|string|null
   *   Returns the security level or NULL.
   */
  public function getZoneSecurityLevel(string $zone_id) {
    $security_level = NULL;

    $cf_auth = $this->getApiAuth();
    if ($cf_auth) {
      $cf_adapter = new Guzzle($cf_auth);
      $cf_firewall_settings = new FirewallSettings($cf_adapter);
      $security_level = $cf_firewall_settings->getSecurityLevelSetting($zone_id);
    }

    return $security_level;
  }

  /**
   * Set Cloudflare zone security level.
   *
   * @param string $zone_id
   *   The Cloudflare zone id.
   * @param string $security_level
   *   The security level.
   *
   * @return bool
   *   Returns the update status.
   */
  public function setZoneSecurityLevel(string $zone_id, string $security_level): bool {
    $status = FALSE;

    $cf_auth = $this->getApiAuth();
    if ($cf_auth) {
      $cf_adapter = new Guzzle($cf_auth);
      $cf_firewall_settings = new FirewallSettings($cf_adapter);
      $status = $cf_firewall_settings->updateSecurityLevelSetting($zone_id, $security_level);
    }

    return $status;
  }

  /**
   * Get Cloudflare configured zone id.
   *
   * @return mixed|null
   *   Returns the zone id.
   */
  public function getZoneId() {
    $config = $this->getStateConfig();
    return !empty($config['cloudflare_zone_id']) ? $config['cloudflare_zone_id'] : NULL;
  }

  /**
   * Determine if zones per language is active.
   *
   * @return bool
   *   Returns status if multi-zone or not.
   */
  public function isMultiZone(): bool {
    return (bool) $this->state->get('cloudflare_multi_zone');
  }

  /**
   * Get Cloudflare zones mapped per language.
   *
   * @return array
   *   Returns array of zones mapped per language.
   */
  public function getLanguageZones(): array {
    return $this->state->get('cloudflare_lang_zones');
  }

  /**
   * Get language domain.
   *
   * @param string $langcode
   *   The language code.
   *
   * @return string|null
   *   Returns the domain for language.
   */
  public function getLanguageDomain(string $langcode): ?string {
    $cf_lang_domains = $this->state->get('cloudflare_lang_domains');
    $domain = NULL;
    foreach ($cf_lang_domains as $cf_domain_langcode => $cf_domain) {
      if ($langcode == $cf_domain_langcode) {
        $domain = $cf_domain;
        break;
      }
    }
    return $domain;
  }

  /**
   * Get zone id for specified language code.
   *
   * @param string $langcode
   *   The language code.
   *
   * @return mixed|null
   *   Returns the zone id for the language.
   */
  public function getLanguageZoneId(string $langcode) {
    $cf_lang_zone_id = NULL;
    $cf_lang_zones = $this->getLanguageZones();
    foreach ($cf_lang_zones as $cf_zone_langcode => $cf_zone_id) {
      if ($cf_zone_langcode == $langcode) {
        $cf_lang_zone_id = $cf_zone_id;
      }
    }
    return $cf_lang_zone_id;
  }

}
